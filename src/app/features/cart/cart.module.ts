import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    SharedModule,
    CommonModule
  ],
  exports: [
    CartComponent
  ]
})
export class CartModule { }
