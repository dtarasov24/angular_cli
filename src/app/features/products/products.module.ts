import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsComponent } from './products.component';

@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    SharedModule,
    CommonModule
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule { }
